/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class CD extends Midia {

    private String artista;
    private int quantidadeFaixas;

    public CD(String titulo, int ano, int tempoDuracao, String comentarios, String artista, int quantidadeFaixas) {
        super(titulo, ano, tempoDuracao, comentarios);
        this.artista = artista;
        this.quantidadeFaixas = quantidadeFaixas;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public int getQuantidadeFaixas() {
        return quantidadeFaixas;
    }

    public void setQuantidadeFaixas(int quantidadeFaixas) {
        this.quantidadeFaixas = quantidadeFaixas;
    }

    protected String mostrarDadosEspecificos() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Artista: " + artista + "\n");
        sb.append("Quantidade Faixas: " + quantidadeFaixas);
        return sb.toString();
    }
}
