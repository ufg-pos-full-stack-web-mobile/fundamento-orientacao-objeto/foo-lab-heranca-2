import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class Teste {

    public static void main(String... args) {
        Lista lista = new Lista();

        lista.add(new CD("Malabarizando", 2015, 3600, "Último CD gravado com a formação original do Charlie Brown Jr", "Charlie Brown Jr", 12 ));
        lista.add(new Filme("O clube da Luta",  2009, 5000, "Clássico baseado no livro de Chunk Palunsk", "David Fincher"));
        lista.add(new Jogo("Super Mario World", 1992, 10000, "Um dos primeiros clássicos do console Super Nintendo", 2, "Super Nintendo"));
        lista.add(new CD("Dos barracos de Maderite aos Castelos de Platina", 2009, 4200, "Grande clássico do grupo Realidade Cruel", "Realidade Cruel", 22));
        lista.add(new Filme("Ex-Machina",  2015, 6800, "Um jovem programador chamado Caleb ganha um concurso e recebe a oportunidade de testar uma inteligência artificial criada por Nathan, um brilhante e recluso bilionário", "Alex Garland"));
        lista.add(new Jogo("Donkey Kong Country 2", 1995, 9000, "Clássico do Super Nintendo", 2, "Super Nintendo"));
        lista.add(new CD("Greasters Hits", 2016, 2900, "Melhores músicas do grupo de humor Hermes e Renato", "Hermes e Renato", 18));
        lista.add(new Filme("Três homens em Conflito",  1968, 8000, "Durante a Guerra Civil Americana, um pistoleiro misterioso e dois estrangeiros decidem juntar suas forças para encontrar um tesouro escondido.", "Sergio Leone"));
        lista.add(new Jogo("Chrono Cross", 1999, 50000, "Clássico do Console PS1", 1, "Playstation One"));
        lista.add(new Filme("Era uma vez no Oeste", 1968, 10500, "Em virtude das terras que possuía serem futuramente a rota da estrada de ferro, um pai e todos os filhos são brutalmente assassinados por um matador profissional.", "Sergio Leone"));

        lista.mostrarDadosMidias();

        System.out.println("Quantidade de CDs: " + lista.qtdCds());
        System.out.println("Quantidade de Filmes: " + lista.qtdFilmes());
        System.out.println("Quantidade de Jogos: " + lista.qtdJogos());

    }
}
