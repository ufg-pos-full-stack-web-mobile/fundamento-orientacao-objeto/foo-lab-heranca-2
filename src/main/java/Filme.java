/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class Filme extends Midia {

    private String diretor;

    public Filme(String titulo, int ano, int tempoDuracao, String comentarios, String diretor) {
        super(titulo, ano, tempoDuracao, comentarios);
        this.diretor = diretor;
    }

    public String getDiretor() {
        return diretor;
    }

    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }

    protected String mostrarDadosEspecificos() {
        return "Diretor: " + diretor;
    }
}
