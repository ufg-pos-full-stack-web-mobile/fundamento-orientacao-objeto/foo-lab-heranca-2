/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class Jogo extends Midia {

    private int numeroJogadores;
    private String plataforma;

    public Jogo(String titulo, int ano, int tempoDuracao, String comentarios, int numeroJogadores, String plataforma) {
        super(titulo, ano, tempoDuracao, comentarios);
        this.numeroJogadores = numeroJogadores;
        this.plataforma = plataforma;
    }

    public int getNumeroJogadores() {
        return numeroJogadores;
    }

    public void setNumeroJogadores(int numeroJogadores) {
        this.numeroJogadores = numeroJogadores;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    protected String mostrarDadosEspecificos() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Número de Jogadores: " + numeroJogadores + "\n");
        sb.append("Plataforma: " + plataforma);
        return sb.toString();
    }
}
