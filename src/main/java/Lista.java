import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class Lista {

    private List<Midia> midias;

    public Lista() {
        midias = new LinkedList<Midia>();
    }

    public void add(Midia midia) {
        midias.add(midia);
    }

    public void mostrarDadosMidias() {
        final Iterator<Midia> iterator = midias.iterator();

        while (iterator.hasNext()) {
            Midia midia = iterator.next();
            System.out.println( midia.mostrarDados() );
            if (midia instanceof Jogo) {
                System.out.println("Jogo executado na plataforma: " + ((Jogo) midia).getPlataforma());
            }
            System.out.println();
        }
    }

    public int qtdCds() {
        int qtdCds = 0;

        final Iterator<Midia> i = midias.iterator();

        while (i.hasNext()) {
            final Midia midia = i.next();
            if (midia instanceof CD) {
                qtdCds++;
            }
        }

        return qtdCds;
    }


    public int qtdFilmes() {
        int qtdFilmes = 0;

        final Iterator<Midia> i = midias.iterator();

        while (i.hasNext()) {
            final Midia midia = i.next();
            if (midia instanceof Filme) {
                qtdFilmes++;
            }
        }

        return qtdFilmes;
    }

    public int qtdJogos() {
        int qtdJogos = 0;

        final Iterator<Midia> i = midias.iterator();

        while (i.hasNext()) {
            final Midia midia = i.next();
            if (midia instanceof Jogo) {
                qtdJogos++;
            }
        }

        return qtdJogos;
    }

}
