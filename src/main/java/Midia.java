/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public abstract class Midia {

    private String titulo;
    private int ano;
    private int tempoDuracao;
    private String comentarios;

    public Midia(String titulo, int ano, int tempoDuracao, String comentarios) {
        this.titulo = titulo;
        this.ano = ano;
        this.tempoDuracao = tempoDuracao;
        this.comentarios = comentarios;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getTempoDuracao() {
        return tempoDuracao;
    }

    public void setTempoDuracao(int tempoDuracao) {
        this.tempoDuracao = tempoDuracao;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String mostrarDados() {
        StringBuilder sb = new StringBuilder();
        sb.append("Título: " + titulo + "\n");
        sb.append("Ano: " + ano + "\n");
        sb.append("Tempo duracao: " + tempoDuracao + " segundos\n");
        sb.append("Comentarios: " + comentarios + "\n");
        sb.append(mostrarDadosEspecificos());

        return sb.toString();
    }

    protected abstract String mostrarDadosEspecificos();
}
